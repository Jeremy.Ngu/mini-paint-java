package pobj.pinboard.editor.tools;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import pobj.pinboard.document.ClipRect;
import pobj.pinboard.editor.EditorInterface;

public class ToolRect implements Tool{

	
	private double departX, departY, finX, finY;
	
	@Override
	public void press(EditorInterface i, MouseEvent e) {
		departX =  e.getX();
		departY =  e.getY();
	}

	@Override
	public void drag(EditorInterface i, MouseEvent e) {
		finX =  e.getX();
		finY =  e.getY();
	}

	@Override
	public void release(EditorInterface i, MouseEvent e) {
		finX =  e.getX();
		finY =  e.getY();
		
		if(departX < finX) {
			if(departY < finY) 
				i.getBoard().addClip(new ClipRect(departX, departY, finX, finY, Color.RED));
			else
				i.getBoard().addClip(new ClipRect(departX, finY, finX, departY, Color.RED));
		}
		else {
			if(departY < finY)
				i.getBoard().addClip(new ClipRect(finX, departY, departX, finY, Color.RED));
			else
				i.getBoard().addClip(new ClipRect(finX, finY, departX, departY, Color.RED));

		}
	}

	@Override
	public void drawFeedback(EditorInterface i, GraphicsContext gc) {
		i.getBoard().draw(gc);
	}

	@Override
	public String getName(EditorInterface editor) {
		
		return "Rectangle Drawing tool";
	}

}
