package pobj.pinboard.editor;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.canvas.GraphicsContext;
import pobj.pinboard.document.Board;
import pobj.pinboard.document.Clip;

public class Selection {
	private List<Clip> selection;
	
	public Selection() {
		selection = new ArrayList<Clip>();
	}
	
	public void select(Board board,double x, double y) {
		selection.clear();
		List<Clip> content = board.getContents();
		
		for(Clip c:content) {
			if(c.isSelected(x, y)) {
				selection.add(c);
			}
		}
	}
	
	public void toogleSelect(Board board, double x, double y) {
		List<Clip> content = board.getContents();
		
		for(Clip c:content) {
			if(c.isSelected(x, y)) {
				if(selection.contains(c)) {
					selection.remove(c);
				}
				else {
					selection.add(c);
				}
			}
		}
	}
	
	public void clear() {
		selection.clear();
	}
	
	public List<Clip> getContents(){
		return selection;
	}
	
	public void drawFeedback(GraphicsContext gc) {
		for(Clip c:selection) {
			c.draw(gc);
		}
	}
	
}
