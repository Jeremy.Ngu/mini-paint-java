package pobj.pinboard.editor;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.*;
import pobj.pinboard.document.Board;
import pobj.pinboard.editor.tools.Tool;
import pobj.pinboard.editor.tools.ToolEllipse;
import pobj.pinboard.editor.tools.ToolRect;

public class EditorWindow implements EditorInterface{
	
	private Stage stage;
	private Board board;
	private Tool drawingTool = new ToolRect();
	private EditorWindow classecourante = this;
	
	public EditorWindow(Stage primaryStage) {
		stage = primaryStage;
	
		board = new Board();
		VBox vbox = new VBox();
		Button rectangle = new Button("Rectangle");
		Button ellipse = new Button("Ellipse");
		ToolBar tool = new ToolBar(rectangle, ellipse);
		Label titreDessin = new Label("Fenetre");
		Separator separateur = new Separator();
		Canvas canvas = new Canvas(800,600);
		
		//Ex 7.2.2 ajout de menus deroulant
		MenuItem creer,close; 
		creer = new MenuItem("New");
		close = new MenuItem("Close");
		
		Menu menu = new Menu("_File");
		menu.getItems().add(creer);
		menu.getItems().add(close);
		
		MenuBar mbar = new MenuBar(menu);
				
		vbox.getChildren().add(mbar);
		vbox.getChildren().add(tool);
		vbox.getChildren().add(canvas);
		vbox.getChildren().add(titreDessin);
		vbox.getChildren().add(separateur);
		
		
		stage.setScene(new javafx.scene.Scene(vbox));
		
		rectangle.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				drawingTool = new ToolRect();
				titreDessin.setText(drawingTool.getName(classecourante));
			}
		});
		
		ellipse.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				drawingTool = new ToolEllipse();
				titreDessin.setText(drawingTool.getName(classecourante));
			}
		});
		
		creer.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				new EditorWindow(new Stage());
			}
		});
		
		close.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				stage.close();
			}
		});
		
		canvas.setOnMousePressed(new EventHandler<Event>() {
			public void handle(Event event) {
				drawingTool.press(classecourante,(MouseEvent) event);
			};
		});
		
		canvas.setOnMouseDragged(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				drawingTool.drag(classecourante, (MouseEvent)event);
				drawingTool.drawFeedback(classecourante, canvas.getGraphicsContext2D());
			}
		});
		
		canvas.setOnMouseReleased(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				drawingTool.release(classecourante, (MouseEvent)event);
				drawingTool.drawFeedback(classecourante, canvas.getGraphicsContext2D());
			}
		});
		stage.show();
		
	}
	@Override
	public Board getBoard() {
		return board;
	}
	@Override
	public Selection getSelection() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public CommandStack getUndoStack() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
