package pobj.pinboard.editor;

import java.util.ArrayList;
import java.util.List;

import pobj.pinboard.document.Clip;

public class Clipboard {
	private List<Clip> copy;
	
	public Clipboard() {
		copy = new ArrayList<Clip>();
	}
	
	
	public void copyToClipboard(List<Clip> clips) {
		copy.clear();
		for(Clip c:clips) {
			copy.add(c);
		}
	}
	
	public List<Clip> copyfromClipboard(){
		return copy;
	}
	
	public void clear() {
		copy.clear();
	}
	
	public boolean isEmpty() {
		return copy.isEmpty();
	}
	

}
