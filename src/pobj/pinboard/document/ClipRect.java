package pobj.pinboard.document;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ClipRect extends AbstractClip{

	
	public ClipRect(double x,double y,double z,double w,Color color) {
		super(x,y,z,w,color);
	}
		
	public void draw(GraphicsContext ctx) {
		ctx.setFill(color);
		ctx.fillRect(left, top, right, bottom);
	}



	@Override
	public Clip copy() {
		return new ClipRect(left,top,right,bottom,color);
	}

}
