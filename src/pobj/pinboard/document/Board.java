package pobj.pinboard.document;

import java.util.*;

import javafx.scene.canvas.GraphicsContext;

public class Board{
	private List<Clip> clips;
	
	public Board() {
		clips = new ArrayList<Clip>();
	}
	
	public List<Clip> getContents(){
		return clips;
	}
	
	public void addClip(Clip clip) {
		clips.add(clip);
	}
	
	public void addClip(List<Clip> clip) {
		for(Clip c:clip) {
			clips.add(c);
		}
	}
	
	public void removeClip(Clip clip) {
		clips.remove(clip);
	}
	
	public void removeClip(List<Clip> clip) {
		for(Clip c:clip) {
			clips.remove(c);
		}
	}
	
	public void draw(GraphicsContext gc) {
		for(Clip c:clips) {
			c.draw(gc);
		}
	}
}
