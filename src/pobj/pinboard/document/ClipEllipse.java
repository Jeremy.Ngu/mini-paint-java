package pobj.pinboard.document;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ClipEllipse extends AbstractClip{

	public ClipEllipse(double x,double y, double z, double w, Color color) {
		super(x,y,z,w,color);
	}
	
	@Override
	public Clip copy() {
		return new ClipEllipse(left,top,right,bottom,color);
	}

	@Override
	public void draw(GraphicsContext ctx) {
		ctx.setFill(color);
		ctx.fillOval(left,top,right,bottom);
	}
	
	@Override
	public boolean isSelected(double x, double y) {
		double cx,cy,rx,ry;
		double test1,test2;
		cx = (left + right)/2.0;
		cy = (top + bottom)/2.0;
		
		rx = (right - left)/2.0;
		ry = (bottom - top)/2.0;
		
		test1 = Math.pow(((x-cx)/rx),2);
		test2 = Math.pow(((y-cy)/ry),2);
		
		if(test1 + test2 <= 1) {
			return true;
		}
		return false;
		
	}



}
