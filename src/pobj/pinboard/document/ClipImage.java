package pobj.pinboard.document;

import java.io.File;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class ClipImage implements Clip{

	private double left, top;
	private Image image;
	
	public ClipImage(double left, double top, File filename) {
		this.left     = left;
		this.top      = top;
		image         = new Image("file://" + filename.getAbsolutePath());
	}

	@Override
	public void draw(GraphicsContext ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getTop() {
		// TODO Auto-generated method stub
		return top;
	}

	@Override
	public double getLeft() {
		// TODO Auto-generated method stub
		return left;
	}

	@Override
	public double getBottom() {
		// TODO Auto-generated method stub
		return top+image.getHeight();
	}

	@Override
	public double getRight() {
		// TODO Auto-generated method stub
		return left+image.getWidth();
	}

	@Override
	public void setGeometry(double left, double top, double right, double bottom) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void move(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isSelected(double x, double y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setColor(Color c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Color getColor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Clip copy() {
		// TODO Auto-generated method stub
		return null;
	}


}
