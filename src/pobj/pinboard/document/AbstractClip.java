package pobj.pinboard.document;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class AbstractClip implements Clip{
	
	protected double top,left,right,bottom;
	protected Color color;
	
	public AbstractClip(double x,double y,double z,double w,Color color) {
		left = x;
		top = y;
		right = z;
		bottom = w;
		this.color = color;
	}
	
	public double getTop() {
		return top;
	}
	public double getRight() {
		return right;
	}
	public double getLeft() {
		return left;
	}
	public double getBottom() {
		return bottom;
	}
	
	public void setGeometry(double left, double top, double right, double bottom) {
		this.left = left;
		this.right = right;
		this.top = top;
		this.bottom = bottom;
	}

	public void move(double x, double y) {
		left = left + x;
		right = right + x;
		top = top + y;
		bottom = bottom + y;
	
	}

	public boolean isSelected(double x, double y) {
		if(x >= left && x <= right) {
			if(y >= top && y <= bottom) {
				return true;
			}
		}
		return false;
	}

	public void setColor(Color c) {
		color = c;
	}

	public Color getColor() {
		return color;
	}

	public abstract Clip copy();
	public abstract void draw(GraphicsContext ctx);
	
}
