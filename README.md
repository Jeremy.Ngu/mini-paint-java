# Mini Paint Editor

## Prerequisites 

- Java 12 or above
- Javafx 12 or above

## Description
Small project of an application capable of drawing figures, draw images.


## Running 

1. Clone / Download the project
2. Import into eclipse (Don't forget to setup the libraries for javafx and setting up the JRE to 12 or above)
2. Run src/pobj/pinboard/editor/EditorMain.java

## Authors 

- Redouane Taoussi
- Jérémy Nguyen